import { sleep } from 'k6'
import http from 'k6/http'

export let options = {
    duration: '1m',
    vus: 100,
};

export default function () {
    http.get('http://gitlab_stef_20210616_prod.surge.sh/');
    sleep(1);
}
